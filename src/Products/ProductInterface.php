<?php
namespace Products;

interface ProductInterface
{
    /**
     * @return mixed
     */
    public function getPrice();

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @return mixed
     */
    public function getDescription();

}