<?php
namespace Products;


class TeaProduct implements ProductInterface
{
    /**
     * @var
     */
    private $price;
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $description;

    public function __construct($price, $name, $description)
    {
        $this->price = $price;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

}