<?php
namespace Factories;

use Products\CapuchinoProduct;

class CapuchinoFactory extends AbstractFactory
{
    private $waterPrice = 0.1;
    private $coffeeBean = 0.5;
    private $milk = 0.8;
    private $marchmello = 1;

    public function getProduct()
    {
        return new CapuchinoProduct($this->countPrice(), $this->getName(), $this->getDescription());

    }
    private function countPrice()
    {
        return $this->waterPrice + $this->coffeeBean + $this->milk + $this->marchmello;
    }
    private function getName()
    {
        return 'SuperPuperCapuchino';
    }

    private function getDescription()
    {
        return 'SuperPuperCapuchinoDescription';
    }
}