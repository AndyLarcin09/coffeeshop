<?php
namespace Factories;

abstract class AbstractFactory
{
    /**
     * @return static
     */
    public static function getFactory()
    {
        $caller = get_called_class();
        return new $caller;
    }

    abstract public function getProduct();

}