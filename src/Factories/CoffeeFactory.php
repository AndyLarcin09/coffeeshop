<?php
namespace Factories;

use Products\CoffeeProduct;

class CoffeeFactory extends AbstractFactory
{
    private $waterPrice = 0.1;
    private $coffeeBean = 0.5;
    private $milk = 0.8;

    public function getProduct()
    {
        return new CoffeeProduct($this->countPrice(), $this->getName(), $this->getDescription());

    }
    private function countPrice()
    {
        return $this->waterPrice + $this->coffeeBean + $this->milk;
    }
    private function getName()
    {
        return 'SuperPuperCoffee';
    }

    private function getDescription()
    {
        return 'SuperPuperCoffeeDescription';
    }


}