<?php
namespace Factories;

use Products\TeaProduct;

class TeaFactory extends AbstractFactory
{
    private $waterPrice = 0.1;
    private $pocketTea = 0.2;

    public function getProduct()
    {
        return new TeaProduct($this->countPrice(), $this->getName(), $this->getDescription());

    }
    private function countPrice()
    {
        return $this->waterPrice + $this->pocketTea;
    }
    private function getName()
    {
        return 'SuperPuperTea';
    }

    private function getDescription()
    {
        return 'SuperPuperTeaDescription';
    }

}