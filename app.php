<?php
require_once 'autoloader.php';

$arr = [\Factories\TeaFactory::class, \Factories\CoffeeFactory::class, \Factories\CapuchinoFactory::class];
foreach ($arr as $factoryClass) {
    $factory = call_user_func([
        $factoryClass, 'getFactory']);
    /**
     * @var \Products\ProductInterface $product
     */
    $product = $factory->getProduct();
    echo
        'Name: ' . $product->getName() . PHP_EOL .
        'Description: ' . $product->getDescription() . PHP_EOL .
        'Price for all: ' . $product->getPrice() .'$' . PHP_EOL;
}
